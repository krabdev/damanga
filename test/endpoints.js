var expect = require('expect');
var request = require('request');

describe('End points test', function() {

    it('expect http://mangafox.me/ to work', function(done) {
        request('http://mangafox.me/', function(err, resp, body) {
            expect(resp.statusCode).toEqual(200);
            done();
        })
    })

    it('expect http://mangafox.me/manga/one_piece to work', function(done) {
        request('http://mangafox.me/manga/one_piece', function(err, resp, body) {
            expect(resp.statusCode).toEqual(200);
            done();
        })
    })
});
