'use strict'

let request = require('request'),
    cheerio = require('cheerio'),
    fixTitle = require('./lib/utils').fixTitle


const http = require('http')
const zlib = require('zlib')



const GetNew = () => {
    return new Promise(
        function (resolve, reject) {
            let list = {}
            request('http://mangafox.me/', (error, resp, body) => {
                if (!error && resp.statusCode == 200) {
                    let $ = cheerio.load(body)
                    $("#new li div.nowrap a").each((i, e) => {
                        let b = $(e)
                        list[i] = b.text()
                    })
                    resolve(list)
                }
            })
        }
    )
}

const GetAnime = (title) => {
    return new Promise(
        (resolve, reject) => {
            request('http://mangafox.me/manga/' + fixTitle(title), (err, resp, body) => {
                if(!err && resp.statusCode == 200) {
                    let $ = cheerio.load(body)
                    let list = []

                    $('#chapters ul.chlist li').each((i, e)=>{

                        let $ = cheerio.load(e)
                        let _href = $('a')[1].attribs['href']
                        let _text
                        try {
                            _text = $('.title.nowrap')[0].children[0].data
                        } catch(e) {
                            _text = ""
                        }

                        list.push({
                            'url': _href,
                            'title': _text
                        })
                    })
               
                    resolve(list.reverse())
                }
            })
        }
    )}



function getGzipped(url) {
    // buffer to store the streamed decompression

    return new Promise(

        (resolve, reject) => {


            // Set the headers
            let headers = {
                "accept-encoding" : "gzip,deflate",
            }

            // Configure the request
            let options = {
                url: url,
                method: 'GET',
                headers: headers,
                encoding: null
            }

            request(options, function(error, response, body){
                if(!error && response.statusCode == 200) {
                    if(response.headers['content-encoding'] == 'gzip'){
                        try {
                            zlib.gunzip(body, function(err, dezipped) {
                                resolve(dezipped.toString())
                            })    
                        } catch(e) {
                            reject(e)
                        }
                    } else {
                        resolve(body)
                    }
                }
            })



        }

    )}

const GetPages = (url) => {
    return new Promise(
        function(resolve, reject) {

            getGzipped(url).then(
                function(urlData) {
                    let $ = cheerio.load(urlData)
                    resolve(($('.l option').length - 2) / 2)
                }
            ).catch(function(e) {
                console.log("ERROR")
                reject(e)
            })
        }
    )
}


function _jaja(dataArray, index) {
    return new Promise(
        (resolve, reject) => {
            GetPages(dataArray[index].url)
                .then(function(d){
                    resolve({
                        data: d,
                        i: index
                    })
                })
                .catch(function(e) {
                    reject({
                        data: e,
                        i: index
                    })
                })
        }
    )}


const GetAnimeInformation = (name) => {
    return new Promise(

        (resolve, reject) => {

            GetAnime(name).then(function(animeData) {

                let superList = []

                function maloop(d, i) {
                    console.log('MA LOOP CALLED - ' + d[i].title + ' - ' + i + ', index:' + i + ' - length: ' + animeData.length)
                    if(animeData.length - 1 > i) {
                        _jaja(d, i)
                            .then(function(jajaData){
                                superList.push({
                                    name: d[i].title,
                                    url: d[i].url,
                                    pages: jajaData.data
                                })
                                let newIndex = i + 1
                                maloop(d, newIndex)

                            })
                            .catch(function(dataData) {
                                let newIndex = dataData.i + 1
                                maloop(d, newIndex)
                            })
                    } else {
                        resolve(superList)
                    }
                }

                maloop(animeData, 0)
            })
        }
    )
}


const DownloadImage = (url) => {
    return new Promise(
        
        (resolve, reject) => {
            getGzipped(url).then(
                function(urlData) {
                    let $ = cheerio.load(urlData)
                    let imgUrl = $('.read_img img')[0].attribs['src']
                    resolve(imgUrl)
                }
            ).catch(function(e) {
                console.log("ERROR")
                reject(e)
            })
        }
        
    )
}

//http://mangafox.me/manga/epic_of_gilgamesh/c000/1.html


DownloadImage('http://mangafox.me/manga/epic_of_gilgamesh/c000/1.html')
    .then((imgUrl)=>{
        console.log(imgUrl)
    })

// GetAnimeInformation('EPIC OF GILGAMESH')
//     .then(function(d) {
//         console.log(d)
//     })

