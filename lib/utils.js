'use strict'

const fixTitle = (title) => {
    let t = title.replace(/[^a-zA-Z\d\s-]/g, '_')
    t = t.replace(/\ /g, '_')
    t = t.replace(/\-/g, '_').toLowerCase()
    t = t.replace(/\___/g,'_')
    t = t.replace(/\__/g,'_')
    if(t.substr(t.length-1) == '_') {
        t = t.substr(0, t.length-1)
    }

    if(t.substr(0, 1) == '_') {
        t = t.substr(1)
    }
    return t
}

module.exports = {fixTitle}